import { Student } from '@prisma/client';
import { GetStudentDto } from './dto/getStudent.dto';
import { StudentDto } from './dto/student.dto';
import { StudentService } from './student.service';
export declare class StudentController {
    private student;
    constructor(student: StudentService);
    createStudent(dto: StudentDto): import(".prisma/client").Prisma.Prisma__StudentClient<Student>;
    getStudentByCardId(cardId: number): Promise<Student>;
    getStudents(id: number): Promise<any[]>;
    getStudent(dto: GetStudentDto): Promise<Student[]>;
}
