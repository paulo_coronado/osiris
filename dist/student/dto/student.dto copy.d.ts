export declare class StudentDto {
    status: boolean;
    full_name: string;
    card_id: number;
    email: string;
}
