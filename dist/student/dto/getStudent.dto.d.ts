export declare class GetStudentDto {
    full_name: string;
    card_id: number;
    email: string;
}
