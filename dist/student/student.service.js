"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.StudentService = void 0;
const common_1 = require("@nestjs/common");
const prisma_service_1 = require("../prisma/prisma.service");
let StudentService = class StudentService {
    constructor(prisma) {
        this.prisma = prisma;
    }
    createStudent(dto) {
        if (typeof dto.status != 'boolean') {
            dto.status = Boolean(dto.status);
        }
        if (typeof dto.card_id != 'number') {
            dto.card_id = Number(dto.card_id);
        }
        return this.prisma.student.create({
            data: dto,
        });
    }
    async getStudentByCardId(cardId) {
        console.log(cardId);
        console.log(typeof cardId);
        const student = await this.prisma.student.findUnique({
            where: {
                card_id: cardId,
            },
        });
        if (!student) {
            throw new common_1.NotFoundException('Unable to find Student with cardId ${cardId}');
        }
        return student;
    }
    async getStudents(params) {
        const { take, dto } = params;
        let students;
        if (Number(dto.id) == 1) {
            students =
                await this.prisma.student.findMany({
                    take,
                    where: {
                        status: true,
                    },
                });
        }
        else {
            students =
                await this.prisma.student.findMany({
                    take,
                    cursor: dto,
                    skip: 1,
                    where: {
                        status: true,
                    },
                });
        }
        await students.push({
            currentCursor: students[students.length - 1].id,
        });
        return students;
    }
    async getStudent(dto) {
        const student = await this.prisma.student.findMany({
            where: {
                AND: [
                    dto.email && { email: dto.email },
                    dto.full_name && { full_name: { contains: dto.full_name } },
                    dto.card_id && { card_id: dto.card_id },
                ]
            }
        });
        if (!student[0]) {
            throw new common_1.NotFoundException('Empty result');
        }
        return student;
    }
};
StudentService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [prisma_service_1.PrismaService])
], StudentService);
exports.StudentService = StudentService;
//# sourceMappingURL=student.service.js.map