"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.StudentController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const jwt_guard_1 = require("../auth/guard/jwt.guard");
const getStudent_dto_1 = require("./dto/getStudent.dto");
const student_dto_1 = require("./dto/student.dto");
const student_service_1 = require("./student.service");
let StudentController = class StudentController {
    constructor(student) {
        this.student = student;
    }
    createStudent(dto) {
        return this.student.createStudent(dto);
    }
    getStudentByCardId(cardId) {
        return this.student.getStudentByCardId(cardId);
    }
    getStudents(id) {
        let dto = {};
        dto.id = id;
        const resultado = this.student.getStudents({ take: 3, dto });
        return resultado;
    }
    getStudent(dto) {
        dto.card_id && (dto.card_id = Number(dto.card_id));
        return this.student.getStudent(dto);
    }
};
__decorate([
    (0, common_1.HttpCode)(common_1.HttpStatus.OK),
    (0, common_1.Post)('create'),
    (0, swagger_1.ApiCreatedResponse)({
        description: 'Created Succesfully',
    }),
    (0, swagger_1.ApiUnprocessableEntityResponse)({
        description: 'Bad Request',
    }),
    (0, swagger_1.ApiForbiddenResponse)({
        description: 'Unauthorized Request',
    }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [student_dto_1.StudentDto]),
    __metadata("design:returntype", void 0)
], StudentController.prototype, "createStudent", null);
__decorate([
    (0, common_1.Get)('/getByCardId/:cardId'),
    (0, swagger_1.ApiOkResponse)({
        description: 'The resource was returned successfully',
    }),
    (0, swagger_1.ApiForbiddenResponse)({
        description: 'Unauthorized Request',
    }),
    (0, swagger_1.ApiNotFoundResponse)({
        description: 'Resource not found',
    }),
    __param(0, (0, common_1.Param)('cardId', common_1.ParseIntPipe)),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", void 0)
], StudentController.prototype, "getStudentByCardId", null);
__decorate([
    (0, common_1.Get)('/getStudents/:cursor_id'),
    (0, swagger_1.ApiOkResponse)({
        description: 'The resource was returned successfully',
    }),
    (0, swagger_1.ApiForbiddenResponse)({
        description: 'Unauthorized Request',
    }),
    (0, swagger_1.ApiNotFoundResponse)({
        description: 'Resource not found',
    }),
    __param(0, (0, common_1.Param)('cursor_id', common_1.ParseIntPipe)),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], StudentController.prototype, "getStudents", null);
__decorate([
    (0, common_1.Get)('/getStudent'),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [getStudent_dto_1.GetStudentDto]),
    __metadata("design:returntype", void 0)
], StudentController.prototype, "getStudent", null);
StudentController = __decorate([
    (0, common_1.UseGuards)(jwt_guard_1.JwtGuard),
    (0, common_1.Controller)('students'),
    __metadata("design:paramtypes", [student_service_1.StudentService])
], StudentController);
exports.StudentController = StudentController;
//# sourceMappingURL=student.controller.js.map