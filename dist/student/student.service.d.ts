import { PrismaService } from '../prisma/prisma.service';
import { GetStudentDto, StudentDto } from './dto';
export declare class StudentService {
    private prisma;
    constructor(prisma: PrismaService);
    createStudent(dto: StudentDto): import(".prisma/client").Prisma.Prisma__StudentClient<import(".prisma/client").Student>;
    getStudentByCardId(cardId: number): Promise<import(".prisma/client").Student>;
    getStudents(params: {
        take: number;
        dto: any;
    }): Promise<any[]>;
    getStudent(dto: GetStudentDto): Promise<import(".prisma/client").Student[]>;
}
