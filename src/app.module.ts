import { Module } from '@nestjs/common';
import {ConfigModule} from '@nestjs/config'
import { AuthModule } from './auth/auth.module';
import { PrismaModule } from './prisma/prisma.module';
import { StudentModule } from './student/student.module';
import { DinningRoomModule } from './dinning-room/dinning-room.module';


@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal:true
    }),
    AuthModule, 
    PrismaModule, StudentModule, DinningRoomModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
