import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerDocumentOptions, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe(
    {
      whitelist:true
    }
  ))

  //Configure Swagger - API Documentation
  const config = new DocumentBuilder().setTitle('Osiris Application')
                      .setDescription("Osiris API Application")
                      .setVersion('v1')
                      .addTag('osiris')
                      .build();
  const document = SwaggerModule.createDocument(app, config);

  SwaggerModule.setup('api', app, document);


  await app.listen(3000);
}
bootstrap();
