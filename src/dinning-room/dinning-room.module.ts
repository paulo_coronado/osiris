import { Module } from '@nestjs/common';
import { DinningRoomController } from './dinning-room.controller';
import { DinningRoomService } from './dinning-room.service';

@Module({
  controllers: [DinningRoomController],
  providers: [DinningRoomService]
})
export class DinningRoomModule {}
