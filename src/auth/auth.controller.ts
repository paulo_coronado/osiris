import { Body, Controller, HttpCode, HttpStatus, Post } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthDto } from './dto';

@Controller('auth')
export class AuthController {

    /**
     * @function Constructor 
     * A service object must be injected in order toprocess the request
     * @param authService 
     */

    constructor(private authService:AuthService){};

    /**
     * @function signin
     * Route to signin the path would be '/auth/signin'
     */
    @HttpCode(HttpStatus.OK)
    @Post('signin')
    signin(@Body() dto:AuthDto){
        return this.authService.signin(dto);
    }

    /**
     * @function signin
     * Route to signup the path would be '/auth/signup'
     */
    @Post('signup')
    signup(@Body() dto:AuthDto){
        return this.authService.signup(dto);
    }
}
