import { ForbiddenException, Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';
import { AuthDto } from './dto';
import * as argon from 'argon2';
import { PrismaClientKnownRequestError } from '@prisma/client/runtime';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';


@Injectable()
export class AuthService {
  constructor(
    private prisma: PrismaService,
    private jwt: JwtService,
    private config: ConfigService,
  ) {}

  /**
   * Verify if is a valid user and create a JWT token
   * @param dto
   */

  async signin(dto: AuthDto) {
    const user =
      await this.prisma.user.findUnique({
        where: {
          email: dto.email,
        },
      });

    if (!user) {
      throw new ForbiddenException(
        'Incorrect credentials',
      );
    }

    const pwmatch = argon.verify(
      user.hash,
      dto.password,
    );

    if (!pwmatch) {
      throw new ForbiddenException(
        'Incorrect credentials',
      );
    }
    return this.signtoken(user.id,user.email);
  }

  //To register new users
  async signup(dto: AuthDto) {
    try {
      const hash = await argon.hash(dto.password);
      const user = await this.prisma.user.create({
        data: {
          email: dto.email,
          hash,
        },
      });
      delete user.hash;
      return user;
    } catch (e) {
      if (
        e instanceof
          PrismaClientKnownRequestError &&
        e.code === 'P2002'
      ) {
        throw new ForbiddenException(
          'Credentials taken',
        );
      } else {
        throw e;
      }
    }
  }

  async signtoken(
    userId: number,
    email: string,
  ): Promise<{ access_token:string }> {

    const payload = {
      sub: userId,
      email,
    };

    const secret = this.config.get('JWT_SECRET');
    const options={
        expiresIn:'365d',
        secret:secret
    }
    const token= await this.jwt.signAsync(payload,options)
    console.log(token);

    return {
        access_token:token
    };
  }
}
