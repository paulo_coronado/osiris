import { ApiProperty } from "@nestjs/swagger";
import { IsBooleanString, IsEmail, IsNotEmpty, IsNumberString, IsOptional, IsString } from "class-validator";

export class GetStudentDto {
  //Class-validator's decorators (optional)
  @IsString()
  @IsNotEmpty()
  @IsOptional()
  full_name: string;

  @IsNumberString()
  @IsNotEmpty()
  @IsOptional()
  card_id: number;

  @ApiProperty()
  @IsEmail()
  @IsNotEmpty()
  @IsOptional()
  email: string;
}