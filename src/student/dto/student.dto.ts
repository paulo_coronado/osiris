import { ApiProperty } from "@nestjs/swagger";
import { IsBooleanString, IsEmail, IsNotEmpty, IsNumberString, IsString } from "class-validator";

export class StudentDto{
    
    //Swagger API documentation (Optional)
    @ApiProperty({
        type: String,
        description: 'Student current status.'
      })
    //Class-validator's decorators (optional)
    @IsBooleanString()
    @IsNotEmpty()
    
    //Field definition
    status:boolean;

    
    @ApiProperty({
        type: String,
        description: 'Student Full Name.'
      })
    @IsString()
    @IsNotEmpty()
    full_name:string;

    @ApiProperty({
        type: String,
        description: 'Student Card Identificacion Number.'
      })
    @IsNumberString()
    @IsNotEmpty()    
    card_id:number;

    @ApiProperty()
    @IsEmail()
    @IsNotEmpty()
    email:string;
}