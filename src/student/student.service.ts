import { Injectable, NotFoundException } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';
import { GetStudentDto, StudentDto } from './dto';

@Injectable()
export class StudentService {
  //Import Prisma

  constructor(private prisma: PrismaService) {}

  /**
   * Create a student given a specific DTO
   * @param dto
   * @returns
   */

  createStudent(dto: StudentDto) {
    if (typeof dto.status != 'boolean') {
      dto.status = Boolean(dto.status);
    }

    if (typeof dto.card_id != 'number') {
      dto.card_id = Number(dto.card_id);
    }
    return this.prisma.student.create({
      data: dto,
    });
  }

  /**
   * Get student by card id
   * @param cardId
   * @returns
   */

  async getStudentByCardId(cardId: number) {
    console.log(cardId);
    console.log(typeof cardId);
    const student =
      await this.prisma.student.findUnique({
        where: {
          card_id: cardId,
        },
      });

    if (!student) {
      throw new NotFoundException(
        'Unable to find Student with cardId ${cardId}',
      );
    }

    return student;
  }

  /**
   * Get students using pagination
   * @param params
   * @returns
   */

  async getStudents(params: {
    take: number;
    dto: any;
  }): Promise<any[]> {
    const { take, dto } = params;

    //First search
    let students: any;
    if (Number(dto.id) == 1) {
      students =
        await this.prisma.student.findMany({
          take,
          where: {
            status: true,
          },
        });
    } else {
      //subsequent searches
      students =
        await this.prisma.student.findMany({
          take,
          cursor: dto,
          skip: 1,
          where: {
            status: true,
          },
        });
    }
    //This object could be used by requested to know the cursor of
    //subsequent searches
    await students.push({
      currentCursor:
        students[students.length - 1].id,
    });
    return students;
  }

  /**
   * Search a student or set of student by
   * full_name, card_id
   */

  async getStudent(dto:GetStudentDto){

    const student =
      await this.prisma.student.findMany({
        where: {
            AND:[
                //Concatenate only the fields that are avalaible in the dto
                dto.email&&{ email:dto.email},
                dto.full_name&&{ full_name:{contains:dto.full_name}},
                dto.card_id&&{ card_id:dto.card_id},
            ]            
        }
      });

    if (!student[0]) {
      throw new NotFoundException(
        'Empty result',
      );
    }
    return student;    
  }
}
