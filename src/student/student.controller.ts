import { Body, Controller, Get, HttpCode, HttpStatus, Param, ParseIntPipe, Post, UseGuards } from '@nestjs/common';
import { ApiCreatedResponse, ApiForbiddenResponse, ApiNotFoundResponse, ApiOkResponse, ApiProperty, ApiUnprocessableEntityResponse } from '@nestjs/swagger';
import { Student } from '@prisma/client';
import { JwtGuard } from '../auth/guard/jwt.guard';
import { GetStudentDto } from './dto/getStudent.dto';
import { StudentDto } from './dto/student.dto';
import { StudentService } from './student.service';

//This module routes will be protected by jwt guard of passport
@UseGuards(JwtGuard)
@Controller('students')
export class StudentController {
  constructor(private student: StudentService) {}

  /**
   * Route to create a student
   * @param dto 
   * @returns 
   */
  @HttpCode(HttpStatus.OK)
  @Post('create')
  //API Documentation
  @ApiCreatedResponse({
    description: 'Created Succesfully',
  })
  @ApiUnprocessableEntityResponse({
    description: 'Bad Request',
  })
  @ApiForbiddenResponse({
    description: 'Unauthorized Request',
  })
  createStudent(@Body() dto: StudentDto) {
    return this.student.createStudent(dto);
  }

  /**
   * Route to get an user by card_id
   * @param cardId 
   * @returns 
   */
  
  @Get('/getByCardId/:cardId')
  //API Documentation
  @ApiOkResponse({
    description:
      'The resource was returned successfully',
  })
  @ApiForbiddenResponse({
    description: 'Unauthorized Request',
  })
  @ApiNotFoundResponse({
    description: 'Resource not found',
  })
  
  //Route declaration. Use a Pipe to validate the data that comes with the request, on
  //error throw an exception
  getStudentByCardId(
    @Param('cardId', ParseIntPipe) cardId: number,
  ) {
    return this.student.getStudentByCardId(
      cardId,
    );
  }

  /**
   * Route to get a list of students
   *  
   * @param id 
   * @returns 
   */

  @Get('/getStudents/:cursor_id')
  //API Documentation
  @ApiOkResponse({
    description:
      'The resource was returned successfully',
  })
  @ApiForbiddenResponse({
    description: 'Unauthorized Request',
  })
  @ApiNotFoundResponse({
    description: 'Resource not found',
  })
  
  getStudents(
    @Param('cursor_id', ParseIntPipe) id: number,
  ):Promise<any[]>{

    let dto:any={};

    dto.id=id;
    
    const resultado=  this.student.getStudents({take:3,dto});  
    return resultado;
  }

  /**
   * Search a student by multiple fields
   */

  @Get('/getStudent')

  getStudent(@Body() dto:GetStudentDto){

    // The following line is equivalent to 
    // if(dto.card_id){ dto.card_id=Number(dto.card_id)}
    dto.card_id&&(dto.card_id=Number(dto.card_id))
    return this.student.getStudent(dto);

  }

  


  
}
