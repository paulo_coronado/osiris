/*
  Warnings:

  - You are about to drop the column `dinnerRoomId` on the `Registration` table. All the data in the column will be lost.
  - You are about to drop the column `menuDinnerRoomId` on the `Registration` table. All the data in the column will be lost.
  - You are about to drop the column `dinnerRoomId` on the `Schedule` table. All the data in the column will be lost.
  - You are about to drop the `DinnerRoom` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `MenuDinnerRoom` table. If the table is not empty, all the data it contains will be lost.
  - Added the required column `diningRoomId` to the `Registration` table without a default value. This is not possible if the table is not empty.
  - Added the required column `menuDiningRoomId` to the `Registration` table without a default value. This is not possible if the table is not empty.
  - Added the required column `diningRoomId` to the `Schedule` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "MenuDinnerRoom" DROP CONSTRAINT "MenuDinnerRoom_dinnerRoomId_fkey";

-- DropForeignKey
ALTER TABLE "MenuDinnerRoom" DROP CONSTRAINT "MenuDinnerRoom_menuId_fkey";

-- DropForeignKey
ALTER TABLE "Registration" DROP CONSTRAINT "Registration_dinnerRoomId_fkey";

-- DropForeignKey
ALTER TABLE "Registration" DROP CONSTRAINT "Registration_menuDinnerRoomId_fkey";

-- DropForeignKey
ALTER TABLE "Schedule" DROP CONSTRAINT "Schedule_dinnerRoomId_fkey";

-- AlterTable
ALTER TABLE "Registration" DROP COLUMN "dinnerRoomId",
DROP COLUMN "menuDinnerRoomId",
ADD COLUMN     "diningRoomId" INTEGER NOT NULL,
ADD COLUMN     "menuDiningRoomId" INTEGER NOT NULL;

-- AlterTable
ALTER TABLE "Schedule" DROP COLUMN "dinnerRoomId",
ADD COLUMN     "diningRoomId" INTEGER NOT NULL;

-- DropTable
DROP TABLE "DinnerRoom";

-- DropTable
DROP TABLE "MenuDinnerRoom";

-- CreateTable
CREATE TABLE "DiningRoom" (
    "id" SERIAL NOT NULL,

    CONSTRAINT "DiningRoom_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "MenuDiningRoom" (
    "id" SERIAL NOT NULL,
    "date" TIMESTAMP(3) NOT NULL,
    "status" BOOLEAN NOT NULL DEFAULT true,
    "menuId" INTEGER NOT NULL,
    "diningRoomId" INTEGER NOT NULL,

    CONSTRAINT "MenuDiningRoom_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "Registration" ADD CONSTRAINT "Registration_diningRoomId_fkey" FOREIGN KEY ("diningRoomId") REFERENCES "DiningRoom"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Registration" ADD CONSTRAINT "Registration_menuDiningRoomId_fkey" FOREIGN KEY ("menuDiningRoomId") REFERENCES "MenuDiningRoom"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Schedule" ADD CONSTRAINT "Schedule_diningRoomId_fkey" FOREIGN KEY ("diningRoomId") REFERENCES "DiningRoom"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "MenuDiningRoom" ADD CONSTRAINT "MenuDiningRoom_diningRoomId_fkey" FOREIGN KEY ("diningRoomId") REFERENCES "DiningRoom"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "MenuDiningRoom" ADD CONSTRAINT "MenuDiningRoom_menuId_fkey" FOREIGN KEY ("menuId") REFERENCES "Menu"("id") ON DELETE CASCADE ON UPDATE CASCADE;
