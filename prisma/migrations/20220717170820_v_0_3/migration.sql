/*
  Warnings:

  - A unique constraint covering the columns `[card_id]` on the table `Student` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `card_id` to the `Student` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Student" ADD COLUMN     "card_id" INTEGER NOT NULL;

-- CreateIndex
CREATE UNIQUE INDEX "Student_card_id_key" ON "Student"("card_id");
