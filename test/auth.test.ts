import * as pactum from 'pactum';
import { AuthDto } from '../src/auth/dto';

//describe is a jest function to group test functions
export function authTest() {
    const dto:AuthDto={
        email:'paulocoronado@gmail.com',
        password:'12345'
      }
      
    
describe('Auth',()=>{
    describe('Signup', ()=>{
     //Tests definition
     it('Signup mail empty', () => {
       const result = pactum
         .spec()
         .post('auth/signup')
         .withBody({
           password: dto.password,
         })
         .expectStatus(400);
       return result;
     });

     it('Signup password empty', () => {
      //Data
      const result = pactum.spec()
        .post('auth/signup')
        .withBody({
          email:dto.email,
         }
          )
        .expectStatus(400); //.inspect();
      return result;
    });

    it('Signup mail incorrect', () => {
      //Data
      const result = pactum.spec()
        .post('auth/signup')
        .withBody({
          email:'no_mail'
         }
          )
        .expectStatus(400); //.inspect();
      return result;
    });

      it('Signup test',()=>{
        //Data        
        const result=pactum.spec().post('auth/signup').withBody(dto).expectStatus(201);//.inspect();
        return result;
      });
    });
    describe('Signin', ()=>{
      //Test definition
      it('Signin test',()=>{
        //Data        
        const result=pactum
        .spec()
        .post('auth/signin')
        .withBody(dto).expectStatus(200)
        .stores('token','access_token'); //.inspect();
        return result;
      });
    })
  })
};