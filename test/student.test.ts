import * as pactum from 'pactum';

//describe is a jest function to group test functions
export function studentTest() {
    const dto:any={
        status:'true',
        full_name:'Lolita Pérez',
        card_id:'1001',
        email:'paulocoronado@gmail.com',
      }
      
    
describe('Student', () => {
  describe('Create', () => {
    //Tests definition
    it('should throw if Status empty', () => {
      const result = pactum
        .spec()
        .post('students/create')
        .withBody({
          full_name: dto.full_name,
          email: dto.email,
          card_id: dto.card_id,
        })
        .withHeaders({
          Authorization: 'Bearer $S{token}',
        })
        .expectStatus(400);
      return result;
    });

    it('should throw if email empty', () => {
      const result = pactum
        .spec()
        .post('students/create')
        .withBody({
          full_name: dto.full_name,
          status: dto.status,
          card_id: dto.card_id,
        })
        .withHeaders({
          Authorization: 'Bearer $S{token}',
        })
        .expectStatus(400);
      return result;
    });

    it('should throw if invalid card id', () => {
      const result = pactum
        .spec()
        .post('students/create')
        .withBody({
          full_name: dto.full_name,
          status: dto.status,
          card_id: '1s34',
        })
        .withHeaders({
          Authorization: 'Bearer $S{token}',
        })
        .expectStatus(400);
      return result;
    });

    it('should create a new student', () => {
      const result = pactum
        .spec()
        .post('students/create')
        .withBody(dto)
        .withHeaders({
          Authorization: 'Bearer $S{token}',
        })
        .expectStatus(200)
        .stores('card_id', 'card_id');;
      return result;
    });

    
  });

  describe('Search', ()=>{

    it('should get a student by card id', () => {
      const result = pactum
        .spec()
        .get('students/getByCardId/{card_id}')
        .withPathParams('card_id', '$S{card_id}')
        .withHeaders({
          Authorization: 'Bearer $S{token}',
        })
        .expectStatus(200)
        .expectBodyContains('$S{card_id}');;
      return result;
    });


  })

  
});
};