
import { INestApplication, ValidationPipe } from '@nestjs/common';
import {Test} from '@nestjs/testing'
import * as pactum from 'pactum';
import { PrismaService } from '../src/prisma/prisma.service';
import { AppModule } from '../src/app.module';
import { authTest } from './auth.test';
import { studentTest } from './student.test';


describe('Osiris App end to end testing (e2e)', ()=>{
  let app: INestApplication;
  let prisma: PrismaService;

  //Create a testing module
  beforeAll(async () => {
    const moduleTest =
      await Test.createTestingModule({
        imports: [AppModule],
      }).compile();

    app = moduleTest.createNestApplication();
    app.useGlobalPipes(
      new ValidationPipe({
        whitelist: true,
      }),
    );
    await app.init();
    //Provides a server to by used py pactum.js
    await app.listen(3333);
    pactum.request.setBaseUrl(
      'http://localhost:3333/',
    );
    prisma = app.get(PrismaService);
    await prisma.cleanDb();
  });

  afterAll(() => {
    app.close();
  });

  //Test specification for each module
  authTest();
  studentTest();
})